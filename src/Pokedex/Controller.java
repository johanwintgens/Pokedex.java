package Pokedex;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

public class Controller
        implements Initializable {

    @FXML
    private ListView<pokemon> listview_pokemon;
    @FXML
    private ImageView image_pokemon;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Define the number of Pokémon to load
        int number_of_pokemon = 151;

        // Connect to SQLite file
        Connection con = null;
        String path = "src/pokedex.sqlite";
        String url = "jdbc:sqlite:" + path;

        try {
            con = DriverManager.getConnection(url);
            System.out.println("Connection established");
        }
        catch (Exception e) {
            System.out.println("Connection failed");
            System.out.println(e.getMessage());
        }

        pokemon pokedex[] = new pokemon[number_of_pokemon];

        // Load Pokémon
        try {
            // Initialize needed variables
            int id;
            String name;
            String l_name;
            File img_file;
            double height;
            double weight;

            // Generate and run query
            assert con != null;
            Statement state = con.createStatement();
            String q_pokemon = "SELECT id, identifier, height, weight FROM pokemon";
            ResultSet r_pokemon = state.executeQuery(q_pokemon);

            // Read extracted data from table "pokemon"
            int i = 0;
            while (r_pokemon.next() && i < number_of_pokemon) {
                id = Integer.parseInt(r_pokemon.getString(1));
                l_name = r_pokemon.getString(2);
                img_file = new File("src/img/" + l_name + ".jpg");
                name = l_name.substring(0, 1).toUpperCase() + l_name.substring(1);
                height = Double.parseDouble(r_pokemon.getString(3))/10;
                weight = Double.parseDouble(r_pokemon.getString(4))/10;

                pokedex[i] = new pokemon();
                pokedex[i].id = id;
                pokedex[i].name = name;
                pokedex[i].height = height;
                pokedex[i].weight = weight;
                pokedex[i].img = new Image(img_file.toURI().toString());
                //pokedex[i].img = new Image("src/img/abra.jpg");
                //pokedex[i].img = new Image("img/" + l_name + ".jpg");

                i++;
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }

        // Load Stats
        try {
            // Initialize needed variables
            stats stat;

            // Generate and run query
            Statement state = con.createStatement();
            String q_stats = "SELECT base_stat FROM pokemon_stats";
            ResultSet r_stats = state.executeQuery(q_stats);

            int i;
            for (i = 0; i < number_of_pokemon; i++) {
                stat = new stats();

                int j = 0;
                while (r_stats.next() && j < 6) {
                    //System.out.println(r_stats.getString(1));

                    switch (j) {
                        case 0:
                            stat.hp = Integer.parseInt(r_stats.getString(1));
                            break;
                        case 1:
                            stat.attack = Integer.parseInt(r_stats.getString(1));
                            break;
                        case 2:
                            stat.defense = Integer.parseInt(r_stats.getString(1));
                            break;
                        case 3:
                            stat.sp_attack = Integer.parseInt(r_stats.getString(1));
                            break;
                        case 4:
                            stat.sp_defense = Integer.parseInt(r_stats.getString(1));
                            break;
                        case 5:
                            stat.speed = Integer.parseInt(r_stats.getString(1));
                            break;
                    }

                    pokedex[i].stats = stat;

                    j++;
                }
            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // Print extracted data
        for (pokemon p : pokedex){
            System.out.println(p.fullString());

            // Result of the following code can be used with /img/scrape_pkmn_imgs.sh
            System.out.print(p.name.toLowerCase() + " ");
        }

        // Load array into program
        ObservableList<pokemon> temp = FXCollections.observableArrayList(pokedex);
        listview_pokemon.setItems(temp);

        listview_pokemon.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<pokemon>() {
            @Override
            public void changed(ObservableValue<? extends pokemon> observable, pokemon oldValue, pokemon newValue) {
                image_pokemon.setImage(newValue.img);
            }
        });
    }
}
