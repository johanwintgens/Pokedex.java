package Pokedex;


import javafx.scene.image.Image;

class pokemon {
    int id;
    String name;
    Image img;
    double height;
    double weight;
    stats stats;

    // --Commented out by Inspection (3/15/17 2:45 AM):public List<Integer> type_id = new ArrayList<>();
    // --Commented out by Inspection (3/15/17 2:45 AM):public List<String> type = new ArrayList<>();
    // --Commented out by Inspection (3/15/17 2:45 AM):public List<Integer> moves_id = new ArrayList<>();
    // --Commented out by Inspection (3/15/17 2:45 AM):public List<String> moves = new ArrayList<>();

    String fullString() {
        return "#" + id + " " + name + " | HP: " + stats.hp + " | Att: " + stats.attack + " | Def: " + stats.defense + " | Spd: " + stats.speed + " | SpAtt: " + stats.sp_attack + " | SpDef: " + stats.sp_defense + " | Weight: " + weight + "kg | Height: " + height + "m";
    }

    @Override
    public String toString() {
        return "#" + id + " " + name;
    }
}
